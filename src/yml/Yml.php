<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 31.10.2018
 * Time: 12:35
 */

namespace werewolf8904\marketplacesexport\yml;


use yii\base\BaseObject;

class Yml extends BaseObject
{

    private $_shop = [];
    private $_currencies = [];
    private $_categories = [];
    private $_offers = [];
    private $_from_charset;
    private $_to_charset;
    private $_eol;


    public function __construct($config = [], $from_charset = 'utf-8', $to_charset = 'utf-8', $eol = "\n")
    {
        parent::__construct($config);
        $this->_from_charset = $from_charset;
        $this->_to_charset = $to_charset;
        $this->_eol = $eol;

    }


    /**
     * Методы формирования YML
     */

    public function setShopArray(array $params)
    {
        foreach ($params as $name => $value) {
            $this->setShop($name, $value);
        }
    }

    /**
     * Формирование массива для элемента shop описывающего магазин
     *
     * @param string $name  - Название элемента
     * @param string $value - Значение элемента
     */
    public function setShop($name, $value)
    {
        $allowed = ['name', 'company', 'url', 'phone', 'platform', 'version', 'agency', 'email'];
        if (\in_array($name, $allowed)) {
            $this->_shop[$name] = $this->prepareField($value);
        }
    }

    /**
     * Подготовка текстового поля в соответствии с требованиями Яндекса
     * Запрещаем любые html-тэги, стандарт XML не допускает использования в текстовых данных
     * непечатаемых символов с ASCII-кодами в диапазоне значений от 0 до 31 (за исключением
     * символов с кодами 9, 10, 13 - табуляция, перевод строки, возврат каретки). Также этот
     * стандарт требует обязательной замены некоторых символов на их символьные примитивы.
     *
     * @param string $text
     *
     * @return string
     */
    private function prepareField($field)
    {
        $field = htmlspecialchars_decode($field);
        $field = strip_tags($field);
        $from = ['"', '&', '>', '<', '\''];
        $to = ['&quot;', '&amp;', '&gt;', '&lt;', '&apos;'];
        $field = str_replace($from, $to, $field);
        if ($this->_from_charset !== $this->_to_charset) {
            $field = mb_convert_encoding($field, $this->_to_charset, $this->_from_charset);
        }
        $field = preg_replace('#[\x00-\x08\x0B-\x0C\x0E-\x1F]+#is', ' ', $field);

        return trim($field);
    }

    /**
     * Валюты
     *
     * @param string       $id   - код валюты (RUR, RUB, USD, BYR, KZT, EUR, UAH)
     * @param float|string $rate - курс этой валюты к валюте, взятой за единицу.
     *                           Параметр rate может иметь так же следующие значения:
     *                           CBRF - курс по Центральному банку РФ.
     *                           NBU - курс по Национальному банку Украины.
     *                           NBK - курс по Национальному банку Казахстана.
     *                           СВ - курс по банку той страны, к которой относится интернет-магазин
     *                           по Своему региону, указанному в Партнерском интерфейсе Яндекс.Маркета.
     * @param float        $plus - используется только в случае rate = CBRF, NBU, NBK или СВ
     *                           и означает на сколько увеличить курс в процентах от курса выбранного банка
     *
     * @return bool
     */
    public function setCurrency($id, $rate = 'CBRF', $plus = 0)
    {
        $allow_id = ['RUR', 'RUB', 'USD', 'BYR', 'KZT', 'EUR', 'UAH'];

        if (!\in_array($id, $allow_id)) {
            return false;
        }
        $allow_rate = ['CBRF', 'NBU', 'NBK', 'CB'];
        if (\in_array($rate, $allow_rate)) {
            $plus = str_replace(',', '.', $plus);
            if (is_numeric($plus) && $plus > 0) {
                $this->_currencies[] = [
                    'id' => $this->prepareField(strtoupper($id)),
                    'rate' => $rate,
                    'plus' => (float)$plus
                ];
            } else {
                $this->_currencies[] = [
                    'id' => $this->prepareField(strtoupper($id)),
                    'rate' => $rate
                ];
            }
        } else {
            $rate = str_replace(',', '.', $rate);
            if (!(is_numeric($rate) && $rate > 0)) {
                return false;
            }
            $this->_currencies[] = [
                'id' => $this->prepareField(strtoupper($id)),
                'rate' => (float)$rate
            ];
        }

        return true;
    }

    /**
     * Категории товаров
     *
     * @param string $name      - название рубрики
     * @param int    $id        - id рубрики
     * @param int    $parent_id - id родительской рубрики
     *
     * @return bool
     */
    public function setCategory($name, $id, $parent_id = 0)
    {
        $id = (int)$id;
        if ($id < 1 || \trim($name) === '') {
            return false;
        }
        if ((int)$parent_id > 0) {
            $this->_categories[$id] = [
                'id' => $id,
                'parentId' => (int)$parent_id,
                'name' => $this->prepareField($name)
            ];
        } else {
            $this->_categories[$id] = [
                'id' => $id,
                'name' => $this->prepareField($name)
            ];
        }

        return true;
    }

    /**
     * Товарные предложения
     *
     * @param array $data - массив параметров товарного предложения
     */
    public function setOffer($data)
    {
        if ($data === null) {
            return false;
        }
        $offer = [];

        $attributes = ['id', 'type', 'available', 'bid', 'cbid', 'param'];
        $attributes = array_intersect_key($data, array_flip($attributes));

        foreach ($attributes as $key => $value) {
            switch ($key) {
                case 'id':
                case 'bid':
                case 'cbid':
                    $value = (int)$value;
                    if ($value > 0) {
                        $offer[$key] = $value;
                    }
                    break;

                case 'type':
                    if (\in_array($value, ['vendor.model', 'book', 'audiobook', 'artist.title', 'tour', 'ticket', 'event-ticket'])) {
                        $offer['type'] = $value;
                    }
                    break;

                case 'available':
                    $offer['available'] = ($value ? 'true' : 'false');
                    break;

                case 'param':
                    if (\is_array($value)) {
                        $offer['param'] = $value;
                    }
                    break;

                default:
                    break;
            }
        }

        $type = $offer['type'] ?? '';

        $allowed_tags = ['url' => 0, 'buyurl' => 0, 'price' => 1, 'wprice' => 0, 'currencyId' => 1, 'xCategory' => 0, 'categoryId' => 1, 'picture' => 0, 'store' => 0, 'pickup' => 0, 'delivery' => 0, 'deliveryIncluded' => 0, 'local_delivery_cost' => 0, 'orderingTime' => 0];

        switch ($type) {
            case 'vendor.model':
                $allowed_tags = array_merge($allowed_tags, ['typePrefix' => 0, 'vendor' => 1, 'vendorCode' => 0, 'model' => 1, 'provider' => 0, 'tarifplan' => 0]);
                break;

            case 'book':
                $allowed_tags = array_merge($allowed_tags, ['author' => 0, 'name' => 1, 'publisher' => 0, 'series' => 0, 'year' => 0, 'ISBN' => 0, 'volume' => 0, 'part' => 0, 'language' => 0, 'binding' => 0, 'page_extent' => 0, 'table_of_contents' => 0]);
                break;

            case 'audiobook':
                $allowed_tags = array_merge($allowed_tags, ['author' => 0, 'name' => 1, 'publisher' => 0, 'series' => 0, 'year' => 0, 'ISBN' => 0, 'volume' => 0, 'part' => 0, 'language' => 0, 'table_of_contents' => 0, 'performed_by' => 0, 'performance_type' => 0, 'storage' => 0, 'format' => 0, 'recording_length' => 0]);
                break;

            case 'artist.title':
                $allowed_tags = array_merge($allowed_tags, ['artist' => 0, 'title' => 1, 'year' => 0, 'media' => 0, 'starring' => 0, 'director' => 0, 'originalName' => 0, 'country' => 0]);
                break;

            case 'tour':
                $allowed_tags = array_merge($allowed_tags, ['worldRegion' => 0, 'country' => 0, 'region' => 0, 'days' => 1, 'dataTour' => 0, 'name' => 1, 'hotel_stars' => 0, 'room' => 0, 'meal' => 0, 'included' => 1, 'transport' => 1, 'price_min' => 0, 'price_max' => 0, 'options' => 0]);
                break;

            case 'event-ticket':
                $allowed_tags = array_merge($allowed_tags, ['name' => 1, 'place' => 1, 'hall' => 0, 'hall_part' => 0, 'date' => 1, 'is_premiere' => 0, 'is_kids' => 0]);
                break;

            default:
                $allowed_tags = array_merge($allowed_tags, ['name' => 1, 'vendor' => 0, 'vendorCode' => 0,'min-quantity'=>0]);
                break;
        }

        $allowed_tags = array_merge($allowed_tags, ['aliases' => 0, 'additional' => 0, 'description' => 0, 'sales_notes' => 0, 'promo' => 0, 'manufacturer_warranty' => 0, 'country_of_origin' => 0, 'downloadable' => 0, 'adult' => 0, 'barcode' => 0]);

        $required_tags = array_filter($allowed_tags);

        if (\count(array_intersect_key($data, $required_tags)) !== \count($required_tags)) {
            return false;
        }

        $data = array_intersect_key($data, $allowed_tags);
//		if (isset($data['tarifplan']) && !isset($data['provider'])) {
//			unset($data['tarifplan']);
//		}

        $allowed_tags = array_intersect_key($allowed_tags, $data);

        // Стандарт XML учитывает порядок следования элементов,
        // поэтому важно соблюдать его в соответствии с порядком описанным в DTD
        $offer['data'] = [];
        foreach ($allowed_tags as $key => $value) {
            $offer['data'][$key] = $this->prepareField($data[$key]);
        }

        $this->_offers[] = $offer;
        return true;
    }

    /**
     * Формирование YML файла
     *
     * @return string
     */
    public function getYml()
    {
        $yml = '<?xml version="1.0" encoding="' . $this->_to_charset . '"?>' . $this->_eol;
        $yml .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">' . $this->_eol;
        $yml .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . $this->_eol;
        $yml .= '<shop>' . $this->_eol;

        // информация о магазине
        $yml .= $this->array2Tag($this->_shop);

        // валюты
        $yml .= '<currencies>' . $this->_eol;
        foreach ($this->_currencies as $currency) {
            $yml .= $this->getElement($currency, 'currency');
        }
        $yml .= '</currencies>' . $this->_eol;

        // категории
        $yml .= '<categories>' . $this->_eol;
        foreach ($this->_categories as $category) {
            $category_name = $category['name'];
            unset($category['name'], $category['export']);
            $yml .= $this->getElement($category, 'category', $category_name);
        }
        $yml .= '</categories>' . $this->_eol;

        // товарные предложения
        $yml .= '<offers>' . $this->_eol;
        foreach ($this->_offers as $offer) {
            $tags = $this->array2Tag($offer['data']);
            unset($offer['data']);
            if (isset($offer['param'])) {
                $tags .= $this->array2Param($offer['param']);
                unset($offer['param']);
            }
            $yml .= $this->getElement($offer, 'offer', $tags);
        }
        $yml .= '</offers>' . $this->_eol;

        $yml .= '</shop>';
        $yml .= '</yml_catalog>';

        return $yml;
    }

    /**
     * Преобразование массива в теги
     *
     * @param array $tags
     *
     * @return string
     */
    private function array2Tag($tags)
    {
        $retval = '';
        foreach ($tags as $key => $value) {
            $retval .= '<' . $key . '>' . $value . '</' . $key . '>' . $this->_eol;
        }

        return $retval;
    }

    /**
     * Фрмирование элемента
     *
     * @param array  $attributes
     * @param string $element_name
     * @param string $element_value
     *
     * @return string
     */
    private function getElement($attributes, $element_name, $element_value = '')
    {
        $retval = '<' . $element_name . ' ';
        foreach ($attributes as $key => $value) {
            $retval .= $key . '="' . $value . '" ';
        }
        $retval .= $element_value ? '>' . $this->_eol . $element_value . '</' . $element_name . '>' : '/>';
        $retval .= $this->_eol;

        return $retval;
    }

    /**
     * Преобразование массива в теги параметров
     *
     * @param array $params
     *
     * @return string
     */
    private function array2Param($params)
    {
        $retval = '';
        foreach ($params as $param) {
            $retval .= '<param name="' . $this->prepareField($param['name']);
            if (isset($param['unit'])) {
                $retval .= '" unit="' . $this->prepareField($param['unit']);
            }
            $retval .= '">' . $this->prepareField($param['value']) . '</param>' . $this->_eol;
        }

        return $retval;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->_categories;
    }

    /**
     * @param array $categories
     */
    public function setCategories(array $categories): void
    {
        $this->_categories = $categories;
    }


    public function filterCategory($category)
    {
        return isset($category['export']);
    }
}