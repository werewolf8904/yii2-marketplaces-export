<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 05.11.2018
 * Time: 12:36
 */

namespace werewolf8904\marketplacesexport;


interface IOfferPrepare
{

    /**
     * @param $offer array|\yii\base\Arrayable
     *
     * @return array
     */
    public function prepareOffer($offer): ?array;

}